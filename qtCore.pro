QT += core gui widgets xml
QT += network websockets serialport
QT += sql
QT += multimedia
QT += script scripttools
QT += webenginewidgets webchannel
QT += concurrent
QT += gui-private

TEMPLATE = lib
Debug: TARGET=qtCoreD
Release: TARGET=qtCore
CONFIG += staticlib

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Default rules for deployment.
unix {
    target.path = $$[QT_INSTALL_PLUGINS]/generic
}
!isEmpty(target.path): INSTALLS += target

DESTDIR = $$PWD/libs
DLLDESTDIR = $$PRJDIR ../bin

INCLUDEPATH += $$PWD/extends/ffmpeg/include

CONFIG(release,debug|release){
    LIBS += $$PWD/extends/irrlicht/libs/Irrlicht.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avcodec.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avdevice.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avfilter.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avformat.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avutil.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/postproc.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/swresample.lib
    LIBS += $$PWD/extends/ffmpeg/lib/swscale.lib
}else{
    LIBS += $$PWD/extends/irrlicht/libs/Irrlicht.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avcodec.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avdevice.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avfilter.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avformat.lib
    LIBS += $$PWD/extends/ffmpeg/lib/avutil.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/postproc.lib
    #LIBS += $$PWD/extends/ffmpeg/lib/swresample.lib
    LIBS += $$PWD/extends/ffmpeg/lib/swscale.lib
}

RESOURCES += \
    libqtcore.qrc

HEADERS += \
    includes/3d/cirrlichtwidget.h \
    includes/QsLog/QsLog.h \
    includes/QsLog/QsLogDest.h \
    includes/QsLog/QsLogDestConsole.h \
    includes/QsLog/QsLogDestFile.h \
    includes/QsLog/QsLogDestFunctor.h \
    includes/QsLog/QsLogDisableForThisFile.h \
    includes/QsLog/QsLogLevel.h \
    includes/breakpad/auto_critical_section.h \
    includes/breakpad/breakpad_types.h \
    includes/breakpad/client_info.h \
    includes/breakpad/crash_generation_client.h \
    includes/breakpad/crash_generation_server.h \
    includes/breakpad/crash_report_sender.h \
    includes/breakpad/exception_handler.h \
    includes/breakpad/guid_string.h \
    includes/breakpad/http_upload.h \
    includes/breakpad/ipc_protocol.h \
    includes/breakpad/minidump_cpu_amd64.h \
    includes/breakpad/minidump_cpu_arm.h \
    includes/breakpad/minidump_cpu_arm64.h \
    includes/breakpad/minidump_cpu_mips.h \
    includes/breakpad/minidump_cpu_ppc.h \
    includes/breakpad/minidump_cpu_ppc64.h \
    includes/breakpad/minidump_cpu_sparc.h \
    includes/breakpad/minidump_cpu_x86.h \
    includes/breakpad/minidump_exception_fuchsia.h \
    includes/breakpad/minidump_exception_linux.h \
    includes/breakpad/minidump_exception_mac.h \
    includes/breakpad/minidump_exception_ps3.h \
    includes/breakpad/minidump_exception_solaris.h \
    includes/breakpad/minidump_exception_win32.h \
    includes/breakpad/minidump_format.h \
    includes/breakpad/minidump_generator.h \
    includes/breakpad/scoped_ptr.h \
    includes/breakpad/string_utils-inl.h \
    includes/common/singleton.h \
    includes/common/common.h \
    includes/common/ctranslate.h \
    includes/common/singleapplication.h \
    includes/database/mssqldataprovider.h \
    includes/database/ndbpool.h \
    includes/database/ndbpool_p.h \
    includes/database/recordset.h \
    includes/js/cscriptmanager.h \
    includes/network/cserialportmanager.h \
    includes/network/ctcpsocketclient.h \
    includes/network/ctcpsocketserver.h \
    includes/network/cudpsocket.h \
    includes/network/httplib.h \
    includes/network/cwebsocketclient.h \
    includes/network/cwebsocketserver.h \
    includes/network/networkframemanager.h \
    includes/network/JQLibrary/jqdeclare.hpp \
    includes/network/JQLibrary/jqhttpserver.h \
    includes/network/JQLibrary/jqnet.h \
    includes/widgets/libframelesswindow.h \
    includes/widgets/libmaskwidget.h \
    includes/network/libhttp.h \
    includes/network/ikcp.h \
    includes/multimedia/cremotevoice.h \
    includes/multimedia/avilib.h \
    includes/multimedia/czipmanager.h \
    includes/widgets/libcwebuimanager.h \
    includes/widgets/libcmessagebox.h

SOURCES += \
    src/3d/cirrlichtwidget.cpp \
    src/breakpad/client_info.cc \
    src/breakpad/crash_generation_client.cc \
    src/breakpad/crash_generation_server.cc \
    src/breakpad/crash_report_sender.cc \
    src/breakpad/exception_handler.cc \
    src/breakpad/guid_string.cc \
    src/breakpad/http_upload.cc \
    src/breakpad/minidump_generator.cc \
    src/breakpad/string_utils.cc \
    src/common/common.cpp \
    src/common/ctranslate.cpp \
    src/common/singleapplication.cpp \
    src/js/cscriptmanager.cpp \
    src/network/cserialportmanager.cpp \
    src/network/ctcpsocketclient.cpp \
    src/network/ctcpsocketserver.cpp \
    src/network/cudpsocket.cpp \
    src/widgets/libcmessagebox.cpp \
    src/network/libhttp.cpp \
    src/QsLog/QsLog.cpp \
    src/QsLog/QsLogDest.cpp \
    src/QsLog/QsLogDestConsole.cpp \
    src/QsLog/QsLogDestFile.cpp \
    src/QsLog/QsLogDestFunctor.cpp \
    src/network/cwebsocketclient.cpp \
    src/network/cwebsocketserver.cpp \
    src/widgets/libframelesswindow.cpp \
    src/widgets/libmaskwidget.cpp \
    src/widgets/webui/libcwebuimanager.cpp \
    src/database/mssqldataprovider.cpp \
    src/database/ndbpool.cpp \
    src/database/ndbpool_p.cpp \
    src/network/networkframemanager.cpp \
    src/network/ikcp.cpp \
    src/database/recordset.cpp \
    src/multimedia/cremotevoice.cpp \
    src/multimedia/avilib.c \
    src/multimedia/czipmanager.cpp \
    src/network/JQLibrary/jqhttpserver.cpp \
    src/network/JQLibrary/jqnet.cpp

FORMS += \
    src/widgets/libcmessagebox.ui
