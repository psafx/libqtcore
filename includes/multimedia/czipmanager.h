#ifndef CZIPMANAGER_H
#define CZIPMANAGER_H

#include <QObject>

#include <private/qzipreader_p.h>
#include <private/qzipwriter_p.h>

class CZipManager : public QObject
{
    Q_OBJECT

public:
    CZipManager(QObject *parent = nullptr);
    ~CZipManager(void);

    /// 压缩指定文件或者文件夹到指定压缩文件中
    bool zipFile(QString filepath,QString decZipFile);
    /// 解压指定压缩文件到指定目录
    bool unzipFile(QString srcZipFile,QString decCatalogue);

signals:
    /// 处理的进度
    void processProcessingprogress(int currentIndex,int totalFilecount,QString currentFile);

private:
    /// 得到指定目录下所有的文件
    int FindFile(const QString& _filePath,QVector<QString> &filelist);
};

#endif // CZIPMANAGER_H
