﻿#ifndef LIB_CWEBUIMANAGER_H
#define LIB_CWEBUIMANAGER_H

#include <QObject>
#include <QWebEngineView>
#include <QtWebChannel>

class CWebUIManager : public QObject
{
    Q_OBJECT

public:
    explicit CWebUIManager(QObject *parent = nullptr);
    ~CWebUIManager();

    ///打开一个url
    Q_INVOKABLE void openUrl(QLayout *pLayout,QString url,bool isShow=true,bool isContentMenu=true);
    ///是否显示网页
    Q_INVOKABLE void setShow(bool isShow);
    ///重新载入网页
    Q_INVOKABLE void reLoadData(void);

    /// 发送数据给网页
    Q_INVOKABLE void sendDataToUrl(const QString data);

signals:
    /// 网页上处理qt中发送的数据
    void onProcessQtSendedData(const QString data);
    /// qt中处理网页上发送的数据
    void UrlSendedData(const QString data);

public slots:
    /// 处理从网页中发送过来的数据
    void sendDataToQt(const QString data);

private:
    QWebEngineView m_webView;
    QWebChannel m_webChannel;
};

#endif // CWEBUIMANAGER_H
