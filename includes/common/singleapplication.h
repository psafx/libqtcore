#ifndef SINGLEAPPLICATION_H
#define SINGLEAPPLICATION_H

#include <QApplication>
#include <QLocalServer>

/** 
 * 保证程序一次只启动一个，启动另外一个相同的程序就提示
 */
class SingleApplication:public QApplication
{
    Q_OBJECT
public:
    SingleApplication(int argc,char **argv);

    bool isRunning();

private slots:
    void newLocalConnection();

private:
    QLocalServer *server;

    bool _isRunning;
};

#endif // SINGLEAPPLICATION_H
