﻿#ifndef CSCRIPTMANAGER_H
#define CSCRIPTMANAGER_H

#include <QObject>
#include <QAction>
#include <QFileSystemWatcher>
#include <QScriptEngine>
#include <QScriptEngineDebugger>

enum DebugType
{
    DEBUGTYPE_AUTO = 0,      // 自动触发调试
    DEBUGTYPE_INTERRUPT,     // 立即触发调试
    DEBUGTYPE_NULL
};

class CScriptManager : public QObject
{
    Q_OBJECT

public:
    explicit CScriptManager(QWidget *parent = nullptr);
    ~CScriptManager();

    ///开启调试
    void EnableDebugger(DebugType pDebugType);
    ///建立一个新的对象
    QScriptValue CreateNewObject(void);
    ///注册一个新的对象
    QScriptValue RegisterNewObject(QString objName,QObject *newobject);
    ///注册一个新的函数
    QScriptValue RegisterNewFunction(QString funcName,QScriptEngine::FunctionSignature newfun);

    ///开始运行指定的脚本文件
    bool RunningScript(QString scriptfile,bool isMonitor=true);
    ///带参数调用某个方法
    QScriptValue CallFunction(QString funcName,QScriptValue params);
    ///得到指定名称的注册对象
    QScriptValue getRegisterObjectByName(QString objName);
    ///重置脚本
    void Reset(void);

signals:
    void ScriptRunningSuccess(void);

public slots:
    void onProcessdirectoryChanged(const QString &path);
    void onProcessfileChanged(const QString &path);

private:
    QScriptEngine m_scriptengine;                           /**< 脚本引擎 */
    QScriptEngineDebugger m_scriptdebugger;                 /**< 脚本调试器 */
    QFileSystemWatcher m_FileSystemWatcher;                 /**< 文件监控工具 */

    QHash<QString,QScriptValue> m_allregisterobjects;       /**< 当前所有的注册的对象 */

    QString m_scriptfile;                                   /**< 要使用的脚本文件 */
    qint16 m_scriptefilechecknum;                           /**< 用于检测脚本文件是否改变 */
    bool m_isMonitor;                                       /**< 是否监控这个脚本文件 */
};

#endif // CSCRIPTMANAGER_H
