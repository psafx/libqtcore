﻿#ifndef CTCPSOCKETCLIENT_H
#define CTCPSOCKETCLIENT_H

#include <QObject>
#include <QTimer>
#include <QByteArray>
#include <QJsonObject>
#include <QtCore/QHash>
#include <QtCore/QByteArray>
#include <QTcpSocket>
#include <QUrl>
#include <QFile>

#include "../common/singleton.h"
#include "../common/common.h"
#include "networkframemanager.h"

/**
 * @brief The CTcpSocket class tcpsocket客户端类
 */
class CTcpSocket : public QTcpSocket
{
    Q_OBJECT

public:
    explicit CTcpSocket(NetworkFrameManager *pNetworkFrameManager=NULL,QObject *parent = nullptr,bool isReConnecting=false,qintptr socketDescriptor=-1);
    ~CTcpSocket();

    /// 设置网络消息处理框架
    inline void SetNetworkFrameManager(NetworkFrameManager *pNetworkFrameManager) { m_NetworkFrameManager = pNetworkFrameManager; }
    /// 连接tcp服务器
    Q_INVOKABLE void connectTcpServer(const QString& ip, quint16 port);
    /// 关闭与tcp服务器的连接
    Q_INVOKABLE void close(void);
    /// 发送json数据
    Q_INVOKABLE void sendJson(QJsonObject mes);
    /// 检测是否连接成功
    bool isConnected(void);
    /// 得到连接的心跳计数
    inline int getHeartCount(void) { return m_HeartCount; }

signals:
    void dataPacketReady(QByteArray socketData);
    void sockDisConnect(qint64, const QString &, quint16, QThread *);//NOTE:断开连接的用户信息，此信号必须发出！线程管理类根据信号计数的

public slots:
    /// 发送数据
    qint64 send(QByteArray& data);
    /// 错误处理
    void error(QAbstractSocket::SocketError socketError);
    /// 断开socket的连接
    void disConTcp(qint64 socketDescriptor);
    /// 发送新用户连接信息
    void newconnectClient(qint64 socketDescriptor, const QString &ip, quint16 port);

private slots:
    /// 处理接收数据
    void OnReadyRead();
    /// 处理连接关闭
    void onDisconnected();
    /// 处理连接成功
    void onConnected();
    /// 处理心跳
    void handleTcpSocketHeartTimeOut();
    /// 处理断开重连
    void handleTcpSocketReconnect();

private:
    /// 解析信息包
    bool parsePacket(void);

private:
    QByteArray m_dataPacket;                    /**< 用于存放接收的数据 */
    bool m_recvDataState;                       /**< 用于接收数据处理 */
    tagPacketHearder m_TcpPacketHearder;     /**< 用于存放每次接收的包头数据 */
    NetworkFrameManager *m_NetworkFrameManager;                         /**< 网络消息处理框架 */
    QTimer m_TcpSocketHeartTimeOutTimer,m_TcpSocketReconnectTimer;      /**< 用于处理心跳和断开重连的定时器 */
    QString m_ServerIP;                         /**< 要连接的服务器的ip */
    int m_ServerPort;                           /**< 要连接的服务器的端口 */
    int m_HeartCount;                           /**< 心跳计数 */
    qintptr socketID;
    QMetaObject::Connection dis;
    bool m_isReConnecting;                      /**< 是否自动重连 */
};

#endif // CWEBSOCKETCLIENT_H
