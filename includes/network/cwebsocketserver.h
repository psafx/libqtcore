#ifndef CWEBSOCKETSERVER_H
#define CWEBSOCKETSERVER_H

#include <QObject>
#include <QTimer>
#include <QFile>
#include <QtCore/QHash>
#include <QtCore/QByteArray>

#include "../common/singleton.h"
#include "../common/common.h"
#include "networkframemanager.h"

QT_FORWARD_DECLARE_CLASS(QWebSocketServer)
QT_FORWARD_DECLARE_CLASS(QWebSocket)

class CWebSocketServer : public QObject, public Singleton<CWebSocketServer>
{
    Q_OBJECT
public:
    explicit CWebSocketServer(QObject *parent = nullptr);
    ~CWebSocketServer();

    /// 打开一个指定端口的服务器
    Q_INVOKABLE void OpenServer(int port);
    /// 关闭服务器
    Q_INVOKABLE void CloseServer(void);
    /// 设置服务器最大支持连接数量,默认为30个连接
    Q_INVOKABLE void setMaxPendingConnections(int maxcount);

    /// 设置网络消息处理框架
    Q_INVOKABLE void SetNetworkFrameManager(NetworkFrameManager *pNetworkFrameManager);
    /// 给指定客户端发送文件(注意点：在网络中使用时一定要设置isExcludeUserInputEvents为false,这个参数是在界面中做防假卡死的)
    Q_INVOKABLE bool sendFile(QWebSocket *pwebsocket,QString filepath,bool isExcludeUserInputEvents=false,QString rootpath="");
    /// 给指定客户端发送字符数据
    Q_INVOKABLE qint64 Send(QWebSocket *pwebsocket,QString msg);
    /// 给指定客户端发送二进制数据
    Q_INVOKABLE qint64 SendByteArray(QWebSocket *pwebsocket,QByteArray &data);
    /// 给所有客户端发送字符数据
    Q_INVOKABLE bool SendAll(QString msg);
    /// 给除了指定客户端的其它客户端发送字符数据
    Q_INVOKABLE bool SendAllOther(QWebSocket *pwebsocket,QString msg);
    /// 给所有客户端发送二进制数据
    Q_INVOKABLE bool SendAllByteArray(QByteArray &data);
    /// 给除了指定客户端的其它客户端发送二进制数据
    Q_INVOKABLE bool SendAllOtherByteArray(QWebSocket *pwebsocket,QByteArray &data);
    /// 是否处理接收文件
    Q_INVOKABLE void setIsProcessRecvFile(bool isProcess,QString recvfiledir="");

    /// 得到当前所有在线的客户端
    Q_INVOKABLE QList<QWebSocket*> getAllClients(void);

Q_SIGNALS:
    void closed();

private Q_SLOTS:
    /// 一个新的连接到达
    void onNewConnection();
    /// 新的字符串消息到达
    void processTextMessage(QString message);
    /// 新的二进制数据消息到达
    void processBinaryMessage(QByteArray message);
    /// 一个客户端断开连接
    void socketDisconnected();
    /// 心跳处理
    void handleWebSocketHeartTimeOut();

private:
    /// 处理文件接收
    void onPrcessRecvFile(QWebSocket *pwebsocket,const QByteArray &data);

private:
    QWebSocketServer *m_pWebSocketServer;                                  /**< 网络服务器 */
    QHash<QWebSocket *,int> m_clients;                                     /**< 保存所有当前连接上的客户端 */
    QTimer m_WebSocketHeartTimeOutTimer;                                   /**< 处理心跳的定时器 */
    NetworkFrameManager *m_NetworkFrameManager;                            /**< 网络处理框架 */

    QByteArray m_recvFileBytes;                                         /**< 用于处理接收到的文件数据 */
    bool m_recvFileState;                                               /**< 接收文件的状态控制 */
    tagFileStruct m_tagFileStruct;                                      /**< 用于文件接收处理 */
    bool m_processrecvFile;                                             /**< 是否处理接收文件 */
    QString m_recvFileSaveDir;                                          /**< 接收文件的保存路径 */
};

#define sCWebSocketServer CWebSocketServer::getSingleton()

#endif // CWEBSOCKETSERVER_H
