﻿#include "../../includes/widgets/libmaskwidget.h"
#include "../../includes/widgets/libframelesswindow.h"

#include <QApplication>
#include <QEvent>
#include <QPainter>
#include <QBitmap>
#include <qDebug>

// 边框阴影区域宽度
#define    BoardShadeWidth                   (6)

MaskWidget::MaskWidget(QWidget *parent)
    : QWidget(parent),
      m_mainWidget(NULL),
      m_topWidget(NULL),
      _shadeRoundEnabled(false),m_shadeRoundEnabled(false)
{
    init();
}

MaskWidget::~MaskWidget()
{

}

/**
 * @brief MaskWidget::setMainWidget 设置主窗口
 * @param widget
 */
void MaskWidget::setMainWidget(QWidget *widget)
{
    if(widget == NULL)
        return;

    widget->installEventFilter(this);
    m_mainWidget = widget;

    FramelessWindow *pFramelessWindow = static_cast<FramelessWindow*>(widget);
    if(pFramelessWindow) setShadeRounded(pFramelessWindow->isShadeRounded());

    connect(m_mainWidget,&QWidget::destroyed,this,[=](){
        m_mainWidget = NULL;
    });
}

/**
 * @brief MaskWidget::setPaletteColor 设置调色板颜色
 * @param color 要设置的颜色
 * @param opacity 不透明度
 */
void MaskWidget::setPaletteColor(const QColor &color,float opacity)
{
    if(!color.isValid())
        return;

    QPalette palette = this->palette();
    palette.setBrush(QPalette::Background,color);
    this->setPalette(palette);

    setWindowOpacity(opacity);
}

/**
 * @brief MaskWidget::setTopWidget 设置顶层窗口
 * @param widget
 */
void MaskWidget::setTopWidget(QWidget *widget)
{
    if(widget == NULL)
        return;

    m_topWidget = widget;
}

/**
 * @brief MaskWidget::setMaskColor 设置遮挡颜色
 * @param color
 */
void MaskWidget::setMaskColor(QColor color)
{
    m_color = color;
    setPaletteColor(m_color,0.6f);
}

void MaskWidget::init()
{
    m_color = QColor(0,0,0);
    setWindowFlags(Qt::FramelessWindowHint | Qt::Tool);
    setPaletteColor(m_color,0.6f);

    m_topWidget = QApplication::activeWindow();
}

void MaskWidget::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    this->setGeometry(m_topWidget->geometry());
}

void MaskWidget::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::WindowStateChange)
    {
        if(this->windowState() == Qt::WindowMaximized)
        {
            _shadeRoundEnabled = false;
        }
        else
        {
            _shadeRoundEnabled = m_shadeRoundEnabled;
        }
    }
}

bool MaskWidget::eventFilter(QObject *watched, QEvent *event)
{
    if(watched == m_mainWidget)
    {
        if(event->type() == QEvent::Show)
        {
            //if(m_topWidget) m_topWidget->show();
            this->show();
        }

        if(event->type() == QEvent::Hide)
        {
            this->hide();
        }
    }

    return QObject::eventFilter(watched,event);
}

void MaskWidget::paintEvent(QPaintEvent *event)
{
    /*QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);

    QPainterPath path2;
    path2.addRoundedRect(BoardShadeWidth, BoardShadeWidth,
                 this->width()-BoardShadeWidth*2,
                 30,
                 10,10);
    QPolygon polygon = path2.toFillPolygon().toPolygon();
    QRegion region(polygon);
    setMask(region);
    painter.fillPath(path2, QBrush(m_color));*/

    if(_shadeRoundEnabled)
    {
        QBitmap bmp(this->size());
        bmp.fill();
        QPainter p(&bmp);
        p.setPen(Qt::NoPen);
        p.setBrush(Qt::black);

        QWidget *pparent = static_cast<QWidget*>(this->parent());

        if(pparent)
        {
            if(pparent->isMaximized())
                p.drawRoundedRect(bmp.rect(),0,0);
            else
                p.drawRoundedRect(QRect(bmp.rect().x()+BoardShadeWidth,bmp.rect().y()+BoardShadeWidth,bmp.rect().width()-BoardShadeWidth*2,bmp.rect().height()-BoardShadeWidth*2),9,9);
        }
        else
        {
            p.drawRoundedRect(QRect(bmp.rect().x()+BoardShadeWidth,bmp.rect().y()+BoardShadeWidth,bmp.rect().width()-BoardShadeWidth*2,bmp.rect().height()-BoardShadeWidth*2),9,9);
        }

        setMask(bmp);
    }
    else
    {
        QBitmap bmp(this->size());
        bmp.fill();
        QPainter p(&bmp);
        p.setPen(Qt::NoPen);
        p.setBrush(Qt::black);

        QWidget *pparent = static_cast<QWidget*>(this->parent());

        if(pparent)
        {
            if(pparent->isMaximized())
                p.drawRoundedRect(bmp.rect(),0,0);
            else
                p.drawRoundedRect(QRect(bmp.rect().x()+BoardShadeWidth,bmp.rect().y()+BoardShadeWidth,bmp.rect().width()-BoardShadeWidth*2,bmp.rect().height()-BoardShadeWidth*2),0,0);
        }
        else
        {
            p.drawRoundedRect(QRect(bmp.rect().x()+BoardShadeWidth,bmp.rect().y()+BoardShadeWidth,bmp.rect().width()-BoardShadeWidth*2,bmp.rect().height()-BoardShadeWidth*2),0,0);
        }

        setMask(bmp);
    }
}
