#include "../../includes/network/cwebsocketclient.h"
#include "QtWebSockets/qwebsocket.h"
#include "../../includes/QsLog/QsLog.h"

#include <QTextCodec>
#include <QJsonObject>
#include <QJsonDocument>
#include <QFileInfo>
#include <QCoreApplication>
#include <QEventLoop>
#include <QByteArray>
#include <QDir>

initialiseSingleton(CWebSocketClientManager);

QT_USE_NAMESPACE

CWebSocketClient::CWebSocketClient(NetworkFrameManager *pNetworkFrameManager,QObject *parent) :
    QObject(parent),
    m_recvFileState(false),
    m_processrecvFile(false),
    m_NetworkFrameManager(pNetworkFrameManager)
{
    connect(&m_webSocket, &QWebSocket::connected, this, &CWebSocketClient::onConnected);
    connect(&m_webSocket, &QWebSocket::disconnected, this, &CWebSocketClient::onDisconnected);
    connect(&m_webSocket, &QWebSocket::textMessageReceived,this, &CWebSocketClient::onTextMessageReceived);
    connect(&m_webSocket, &QWebSocket::binaryMessageReceived, this, &CWebSocketClient::onBinaryReceived);

    QObject::connect(&m_WebSocketHeartTimeOutTimer, &QTimer::timeout, this, &CWebSocketClient::handleWebSocketHeartTimeOut);
    m_WebSocketHeartTimeOutTimer.setInterval(5000);

    QObject::connect(&m_WebSocketReconnectTimer, &QTimer::timeout, this, &CWebSocketClient::handleWebSocketReconnect);
    m_WebSocketReconnectTimer.setInterval(5000);
}

CWebSocketClient::~CWebSocketClient()
{
    m_webSocket.close();
}

/**
 * @brief CWebSocketClient::Open 打开指定地址的网络连接
 * @param url 要打开的网址
 */
void CWebSocketClient::Open(QUrl url)
{
    m_websocketurl = url;
    m_webSocket.open(m_websocketurl);

    QLOG_INFO()<<"CWebSocketClient::Open:"<<url;
}

/**
 * @brief CWebSocketClient::Send 发送字符串
 * @param msg 要发送的字符串
 * @return 返回发送成功的字符串长度
 */
qint64 CWebSocketClient::Send(QString msg)
{
    if(msg.isEmpty())
        return 0;

    return m_webSocket.sendTextMessage(msg);
}

/**
 * @brief CWebSocketClient::SetNetworkFrameManager 设置网络消息处理框架
 * @param pNetworkFrameManager 要设置的消息处理框架
 */
void CWebSocketClient::SetNetworkFrameManager(NetworkFrameManager *pNetworkFrameManager)
{
    if(pNetworkFrameManager == NULL)
        return;

    m_NetworkFrameManager = pNetworkFrameManager;
}

/**
 * @brief CWebSocketClient::sendFile 发送文件
 *
 * @param isExcludeUserInputEvents 是否要做防卡死，如果是在网络中处理就不用，在界面上处理要做防卡死处理，初始是要做防卡死处理的
 * @param filepath 要发送的文件的完整路径
 * @param rootpath 主路径，主要去除发送的文件路径，得到文件的相对路径
 * @return 如果文件发送成功返回真，否则返回假
 */
bool CWebSocketClient::sendFile(QString filepath,bool isExcludeUserInputEvents,QString rootpath)
{
    if(filepath.isEmpty())
        return false;

    QFileInfo pfileinfo(filepath);
    if(!pfileinfo.exists())
    {
        QLOG_ERROR()<<filepath<<" is exist.";
        return false;
    }

    QFile m_sendfile;
    m_sendfile.setFileName(filepath);

    if(!m_sendfile.open(QIODevice::ReadOnly))
    {
        QLOG_ERROR()<<filepath<<" open fail.";
        return false;
    }

    if(rootpath.isEmpty())
    {
        int pos = filepath.lastIndexOf("/");
        if(pos > 0)
            rootpath = filepath.mid(0,pos+1);
    }

    qint64 m_sendsize,m_totalfilesize;                                  /**< 当前发送的文件的数据大小和文件总大小 */

    QByteArray tmpByteArray = qCompress(m_sendfile.readAll());

    m_sendfile.close();

    m_totalfilesize = tmpByteArray.size();
    m_sendsize=0;
    quint16 pdecchecknum = qChecksum(tmpByteArray.constData(),tmpByteArray.size());

    QString tmpRealFilePath = filepath.mid(rootpath.size());

    tagFileStruct ptagFileStruct;
    memset(&ptagFileStruct,0,sizeof(ptagFileStruct));

    strncpy(ptagFileStruct.fileName,tmpRealFilePath.toUtf8().data(),MAX_FILENAME);
    ptagFileStruct.compressfileSize = tmpByteArray.size();
    ptagFileStruct.srcfileSize = pfileinfo.size();
    ptagFileStruct.checknum = pdecchecknum;

    QByteArray pHeardBytes = QByteArray((char*)&ptagFileStruct,sizeof(ptagFileStruct));

    if(sendBinaryMessage(pHeardBytes) != pHeardBytes.size())
    {
        QLOG_ERROR()<<filepath<<" heard send fail.";
        return false;
    }

    do
    {       
        if(m_webSocket.state() != QAbstractSocket::ConnectedState)
        {
            QLOG_ERROR()<<"CWebSocketClient::sendFile send data fail:QAbstractSocket::ConnectedState.";
            return false;
        }

        QByteArray psendbytearray;

        if(tmpByteArray.size()-m_sendsize < BUF_SIZE)
            psendbytearray = tmpByteArray.mid(m_sendsize);
        else
            psendbytearray = tmpByteArray.mid(m_sendsize,BUF_SIZE);

        m_sendsize += sendBinaryMessage(psendbytearray);

        if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessSendFile(&m_webSocket,filepath,m_sendsize,m_totalfilesize);

        if(isExcludeUserInputEvents)
            QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);
    }
    while(m_sendsize < m_totalfilesize);

    return true;
}

/**
 * @brief CWebSocketClient::sendBinaryMessage 发送二进制数据
 * @param data 要发送的二进制数据
 * @return 返回发送成功的数据长度
 */
qint64 CWebSocketClient::sendBinaryMessage(const QByteArray &data)
{
    if(data.isEmpty())
        return 0;

    return m_webSocket.sendBinaryMessage(data);
}

/**
 * @brief CWebSocketClient::Close 关闭连接
 */
void CWebSocketClient::Close(void)
{
    m_WebSocketHeartTimeOutTimer.stop();
    m_WebSocketReconnectTimer.stop();
    m_webSocket.close();
}

/**
 * @brief CWebSocketClient::onConnected 连接成功后的处理
 */
void CWebSocketClient::onConnected()
{
    m_WebSocketHeartTimeOutTimer.start();
    m_WebSocketReconnectTimer.stop();

    QLOG_INFO()<<"CWebSocketClient::onConnected";

    if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessConnectedNetMes(&m_webSocket);
}

/**
 * @brief CWebSocketClient::onDisconnected 连接断开后的处理
 */
void CWebSocketClient::onDisconnected()
{
    m_WebSocketHeartTimeOutTimer.stop();
    m_WebSocketReconnectTimer.start();

    if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessDisconnectedNetMes(&m_webSocket);
}

/**
 * @brief CWebSocketClient::handleWebSocketReconnect 处理客户端断开后重连
 */
void CWebSocketClient::handleWebSocketReconnect()
{
    if(m_websocketurl.isValid())
    {
        QLOG_INFO()<<"CWebSocketClient::handleWebSocketReconnect()";

        m_webSocket.open(m_websocketurl);
    }
}

/**
 * @brief CWebSocketClient::handleWebSocketHeartTimeOut 处理心跳消息
 */
void CWebSocketClient::handleWebSocketHeartTimeOut()
{
    m_webSocket.sendTextMessage("heart");
}

/**
 * @brief CWebSocketClient::onTextMessageReceived 处理接收到字符串时的情况
 * @param message 接收到的字符串
 */
void CWebSocketClient::onTextMessageReceived(QString message)
{
    QLOG_INFO()<<"CWebSocketClient::onTextMessageReceived:"<<message;

    if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessNetText(&m_webSocket,message);
}

/**
 * @brief CWebSocketClient::onBinaryReceived 处理接收到二进制数据的情况
 * @param message 接收到的二进制数据
 */
void CWebSocketClient::onBinaryReceived(QByteArray message)
{
    // 是否处理接收文件
    if(m_processrecvFile) onPrcessRecvFile(&m_webSocket,message);

    if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessNetBinary(&m_webSocket,message);
}

/// 是否处理接收文件
void CWebSocketClient::setIsProcessRecvFile(bool isProcess,QString recvfiledir)
{
    m_processrecvFile = isProcess;
    m_recvFileSaveDir = recvfiledir;

    if(m_processrecvFile) m_recvFileState=false;
}

/**
 * @brief onPrcessRecvFile 处理文件接收
 * @param pwebsocket 要处理的客户端
 * @param data 要处理的数据
 */
void CWebSocketClient::onPrcessRecvFile(QWebSocket *pwebsocket,const QByteArray &data)
{
    m_recvFileBytes.push_back(data);

    while(!m_recvFileBytes.isEmpty())
    {
        if(!m_recvFileState &&
            m_recvFileBytes.size() >= sizeof(tagFileStruct))
        {
            memcpy(&m_tagFileStruct,m_recvFileBytes.constData(),sizeof(m_tagFileStruct));

            QLOG_INFO()<<"CWebSocketClient::onPrcessRecvFile filestruct:"<<QString(m_tagFileStruct.fileName)<<" "<<m_tagFileStruct.compressfileSize<<" "<<m_tagFileStruct.checknum;
            //qDebug()<<"filestruct:"<<QString(m_tagFileStruct.fileName)<<" "<<m_tagFileStruct.compressfileSize<<" "<<m_tagFileStruct.checknum;

            if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessRecvFile(pwebsocket,QString(m_tagFileStruct.fileName),"",STATE_START);

            m_recvFileBytes.remove(0,sizeof(m_tagFileStruct));
            m_recvFileState = true;
        }

        if(!m_recvFileState || m_recvFileBytes.size() < m_tagFileStruct.compressfileSize)
            break;

        // 得到当前文件数据
        QByteArray precvFileData = m_recvFileBytes.mid(0,m_tagFileStruct.compressfileSize);
        m_recvFileBytes.remove(0,m_tagFileStruct.compressfileSize);

        // 获取文件校验码
        quint16 pdecchecknum = qChecksum(precvFileData.constData(),precvFileData.size());

        // 解压文件
        QByteArray precvFileBytes = qUncompress(precvFileData);

        QLOG_INFO()<<"CWebSocketClient::onPrcessRecvFile recv:"<<QString(m_tagFileStruct.fileName)<<" "<<pdecchecknum<<" finished.";
        //qDebug()<<"recv:"<<QString(m_tagFileStruct.fileName)<<" "<<pdecchecknum<<" "<<precvFileData.size()<<" "<<m_tagFileStruct.compressfileSize<<" finished.";

        QString appfiledir = QCoreApplication::applicationDirPath() + QString("/tempData/tempData.tmp");

        if(m_tagFileStruct.checknum == pdecchecknum)
        {
            if(!m_recvFileSaveDir.isEmpty())
                appfiledir = m_recvFileSaveDir + QString("/tempData.tmp");

            QString tmpFileDirPath = appfiledir.mid(0,appfiledir.lastIndexOf(tr("/")));

            QDir dir(tmpFileDirPath);
            if(!dir.exists())
            {
                if(!dir.mkpath(tmpFileDirPath))
                {
                    QLOG_ERROR()<<"CWebSocketClient::onPrcessRecvFile:"<<tmpFileDirPath<<" create fail.";
                    m_recvFileState=false;
                    continue;
                }
            }

            QFile precvFile(appfiledir);
            if(precvFile.open(QIODevice::WriteOnly))
            {
                qint64 tmprecvfilesize = precvFile.write(precvFileBytes);

                if(tmprecvfilesize != m_tagFileStruct.srcfileSize)
                {
                    if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessRecvFile(pwebsocket,QString(m_tagFileStruct.fileName),appfiledir,STATE_SIZE);

                    QLOG_ERROR()<<"CWebSocketClient::onPrcessRecvFile write file:"<<appfiledir<<" "<<tmprecvfilesize<<" "<<m_tagFileStruct.srcfileSize<<" "<<precvFileBytes.size()<<" error.";
                    precvFile.close();
                    m_recvFileState=false;
                    continue;
                }

                precvFile.close();

                if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessRecvFile(pwebsocket,QString(m_tagFileStruct.fileName),appfiledir,STATE_SUCCESS);
            }
            else
            {
                QLOG_ERROR()<<"CWebSocketClient::onPrcessRecvFile recv:"<<appfiledir<<" "<<pdecchecknum<<" "<<m_tagFileStruct.checknum<<" write error.";

                if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessRecvFile(pwebsocket,QString(m_tagFileStruct.fileName),appfiledir,STATE_NOTWRITE);
                m_recvFileState=false;
                continue;
            }
        }
        else
        {
            if(m_NetworkFrameManager) m_NetworkFrameManager->OnProcessRecvFile(pwebsocket,QString(m_tagFileStruct.fileName),appfiledir,STATE_CHECKNUM);
            QLOG_ERROR()<<"CWebSocketClient::onPrcessRecvFile recv:"<<QString(m_tagFileStruct.fileName)<<" "<<pdecchecknum<<" "<<m_tagFileStruct.checknum<<" error.";
        }

        m_recvFileState=false;
    }
}

CWebSocketClientManager::CWebSocketClientManager()
{

}

CWebSocketClientManager::~CWebSocketClientManager()
{
    clearAllClients();
}

/**
 * @brief CWebSocketClientManager::clearAllClients 清除所有的客户端
 */
void CWebSocketClientManager::clearAllClients(void)
{
    QHash<QString,CWebSocketClient*>::iterator iter = m_WebSocketClients.begin();
    for(;iter != m_WebSocketClients.end();++iter)
    {
        if((*iter)) delete (*iter);
        (*iter) = NULL;
    }

    m_WebSocketClients.clear();
}

/**
 * @brief CWebSocketClientManager::addClient 添加一个客户端
 * @param clientName 要添加的客户端的名称
 * @param pClient 要添加的客户端
 * @return 如果客户端添加成功返回真，否则返回假
 */
bool CWebSocketClientManager::addClient(QString clientName,CWebSocketClient* pClient)
{
    if(clientName.isEmpty() || pClient == NULL)
        return false;

    CWebSocketClient *pReturnClient = getClient(clientName);
    if(pReturnClient)
        return false;

    m_WebSocketClients[clientName] = pClient;

    return true;
}

/**
 * @brief CWebSocketClientManager::getClient 得到一个客户端
 * @param clientName 要得到的客户端的名称
 * @return 如果存在这个客户端返回这个客户端，否则返回NULL
 */
CWebSocketClient* CWebSocketClientManager::getClient(QString clientName)
{
    QHash<QString,CWebSocketClient*>::iterator iter = m_WebSocketClients.find(clientName);
    if(iter != m_WebSocketClients.end())
        return (*iter);

    return NULL;
}

/// 清除指定的客户端
bool CWebSocketClientManager::deleteClient(QString clientName)
{
    if(clientName.isEmpty() || m_WebSocketClients.isEmpty())
        return false;

    QHash<QString,CWebSocketClient*>::iterator iter = m_WebSocketClients.find(clientName);
    if(iter != m_WebSocketClients.end())
    {
        delete (*iter);
        (*iter) = NULL;

        m_WebSocketClients.erase(iter);

        return true;
    }

    return false;
}

