#include "../../includes/common/ctranslate.h"
#include "../../includes/QsLog/QsLog.h"

#include <QFile>
#include <QVector>
#include <QXmlStreamWriter>
#include <QXmlStreamReader>
#include <QtAlgorithms>
#include <QCoreApplication>
#include <qDebug>

initialiseSingleton(CTranslate);

CTranslate::CTranslate(QObject *parent,int curLanguage)
    : m_currentSelectedLanguage(curLanguage)
{

}

CTranslate::~CTranslate()
{

}

/**
 * @brief CTranslate::loadEntrys 导入翻译词汇配置文件
 * @param filepath 文件路径
 * @return 如果配置文件加载成功返回真，否则返回假
 */
bool CTranslate::loadEntrys(QString filepath)
{
    if(filepath.isEmpty())
        return false;

    QFile file(filepath);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Truncate | QIODevice::Text))
    {
        QLOG_ERROR()<<filepath<<" open fail.";
        return false;
    }

    m_languagetypes.clear();
    m_entrys.clear();

    QXmlStreamReader reader(&file);

    while(!reader.atEnd() && !reader.hasError())
    {
        if(reader.isStartElement())
        {
            if(reader.name() == "language")
            {
                QXmlStreamAttributes attributes = reader.attributes();

                QString pname = attributes.value("name").toString();

                m_languagetypes.push_back(pname);
            }
            else if(reader.name() == "entry")
            {
                QXmlStreamAttributes attributes = reader.attributes();

                QString pentryname = attributes.value("name").toString();

                QVector<QString> ptranslates;

                reader.readNext();

                while(!reader.atEnd() && !reader.hasError())
                {
                    if(reader.isStartElement())
                    {
                        if(reader.name() == "translate")
                        {
                            QXmlStreamAttributes attributes = reader.attributes();

                            QString pvalue = attributes.value("value").toString();

                            ptranslates.push_back(pvalue);
                        }
                        else
                        {
                            break;
                        }
                    }

                    reader.readNext();
                }

                m_entrys[pentryname] = ptranslates;

                continue;
            }
        }

        QCoreApplication::processEvents(QEventLoop::ExcludeUserInputEvents);

        reader.readNext();
    }

    file.close();

    if(m_languagetypes.isEmpty() || m_entrys.isEmpty())
    {
        QLOG_ERROR()<<filepath<<" load fail.";
        return false;
    }

    if(m_currentSelectedLanguage >= m_languagetypes.size())
        m_currentSelectedLanguage = 0;

    QLOG_INFO()<<filepath<<" load successed.";

    emit loadEntrysFinished();

    return true;
}

/**
 * @brief CTranslate::SwitchLanguage 切换语言
 * @param languageIndex 如果为-1就切换到下一种语言，否则切换到指定的语言
 */
void CTranslate::SwitchLanguage(int languageIndex)
{
    if(languageIndex == -1)
    {
        m_currentSelectedLanguage = (m_currentSelectedLanguage + 1) % m_languagetypes.size();
    }
    else
    {
        if(languageIndex < 0 ||
                languageIndex >= m_languagetypes.size() ||
                m_currentSelectedLanguage == languageIndex)
            return;

        m_currentSelectedLanguage = languageIndex;
    }

    QLOG_INFO()<<"SwitchLanguage:"<<QString::asprintf("%d.",languageIndex);

    emit switchLanguageFinished();
}

/**
 * @brief CTranslate::getEntryTranslate 得到指定名称的翻译
 * @param entryName 要得到翻译的词汇
 * @return 返回相关的翻译
 */
QString CTranslate::getEntryTranslate(QString entryName)
{
    if(entryName.isEmpty() ||
            m_currentSelectedLanguage < 0 ||
            m_currentSelectedLanguage >= m_languagetypes.size())
    {
        QLOG_ERROR()<<"getEntryTranslate: "<<entryName<<" fail.";
        return "";
    }

    QHash<QString,QVector<QString>>::iterator iter = m_entrys.find(entryName);
    if(iter == m_entrys.end())
    {
        QLOG_ERROR()<<"getEntryTranslate: "<<entryName<<" can't find.";
        return "";
    }

    if(m_currentSelectedLanguage >= (*iter).size())
    {
        QLOG_ERROR()<<"getEntryTranslate: "<<entryName<<" out of range.";
        return "";
    }

    return (*iter)[m_currentSelectedLanguage];
}

